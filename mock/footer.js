const tableData1 = [
  {
    date: "2016-05-08",
    name: "张大豹",
    address: "mock返回的数据",
  },
  {
    date: "2016-05-08",
    name: "张大豹",
    address: "mock返回的数据",
  },
  {
    date: "2016-05-08",
    name: "张大豹",
    address: "mock返回的数据",
  },
];

module.exports = [
  {
    url: "/footer/formdata",
    type: "get",
    response: (config) => {
      return {
        code: 20000,
        data: {
          total: tableData1.length,
          items: tableData1,
        },
      };
    },
  },
];
