import request from '@/utils/request'

export function getFooterData(params) {
  return request({
    url: '/footer/formdata',
    method: 'get',
    params
  })
}
